README
# FlockIt API
V 1.0

Api que permite un login con uso de jwt. 
Y conocer datos de las provincias de argentina utilizando api externa del gobierno.

## Documentación
[Documentación postman] (https://documenter.getpostman.com/view/11953718/VV56LsFw) En este link se encuentra la documentacion de postman 

## Environment Variables
Para correr el proyecto se debe crear un archivo .ENV en la raiz del proyecto con estas variables(tambien se incluye en el proyecto un archivo .env de ejemplo):

## Aclaraciones
Primero se debe correr el sigiuente comando para que instale las librerías necesarias:
* npm install

Luego se debe crear una base de datos con el nombre flock-it y agregar al archivo .env los datos correspondientes para la autenticacion.
Luego se debe correr el comando de sequelize-cli para que los seeders llenen la bd.

* npx sequelize-cli db:seed:all

Y luego para correr la api 
* npm run start

En la documentacion de postman estan los datos para loguearse con el usuario.