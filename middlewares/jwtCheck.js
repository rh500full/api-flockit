const jwt = require('jsonwebtoken');
const { secret_key } = require('../config/index');
const { getUserById } = require('../controllers/users');

const jwtCheck = async function(req,res,next){
   const token = req.headers['authorization'];
   try {
      if(!token){
         throw new Error('No token provided');
      }
      const decoded = jwt.verify(token,secret_key);
      const user = getUserById(decoded.id);
      if(!user) throw new Error("UserId don't exists");
      next();
   }
   catch(e){
      next(e);
   }
};

module.exports = jwtCheck