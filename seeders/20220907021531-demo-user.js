'use strict';
const bcrypt = require('bcrypt');
const { DataTypes } = require('sequelize');
let password = bcrypt.hashSync('1234', 10);

module.exports = {
   up: async (queryInterface, Sequelize) => {
      await queryInterface.createTable('Users', {
         userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
         },
         name: DataTypes.STRING,
         lastName: DataTypes.STRING,
      
         userName: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
         },
         password: {
            type: DataTypes.STRING,
            allowNull: false,
         },
         active: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1
         }
      });

      return await queryInterface.bulkInsert('Users', [{
       name: 'ale',
       lastName: 'alaniz',
       userName: 'rh500',
       password: password,
       active: 1
     }]);
   },
   down: (queryInterface, Sequelize) => {
     return queryInterface.bulkDelete('Users', null, {});
   }
 };
