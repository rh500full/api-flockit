const { DataTypes } = require('sequelize');
const sequelize = require('../database/index');
const bcrypt = require('bcryptjs');

const Users = sequelize.define('Users',{
   userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
   },
   name: DataTypes.STRING,
   lastName: DataTypes.STRING,

   userName: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
   },
   password: {
      type: DataTypes.STRING,
      allowNull: false,
   },
   active: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
   }
},{
   timestamps: false,
});

Users.encryptPassword = async password =>{
   const hash = await bcrypt.hash(password,10);
   return hash;
}

Users.decryptPassword = (password,hash) => {
   return bcrypt.compare(password,hash);
}

const sync = async () =>{
   try{
      await Users.sync({alter:true});
   }
   catch(error){
      console.log("Error en sync model user",error);
   }
}

sync();

module.exports = Users;