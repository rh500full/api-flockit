const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const Users = require('../models/Users');
const { secret_key } = require('../config/index');

const signInUser = async(req,res,next)=>{
   try {
      let { userName, password } = req.body;
      
      const user = await Users.findOne({where: {userName}})
      if(user == null){
         res.status(500).json({message:'Usuario no encontrado'});
      }
      const validated = await bcrypt.compare(password,user.password)
      if(validated){
         const token = await jwt.sign({ id: user.id },secret_key);
         let data = {
            msg : "OK",
            token
         }
         res.status(200).json(data)
      }
      else{
         res.status(500).json({message:'Contraseña incorrecta'});
      }
   } catch (e) {
      
   }
}

const getUserById = async(userId)=>{
   try{
      const user = await Users.findByPk(userId);
      return user;
   }
   catch(e){
      next(e);
   }
}

module.exports = {
   signInUser,
   getUserById,
}