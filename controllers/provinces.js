const axios = require('axios');

const getLatLang = async(req,res,next)=>{
      const { provinceName } = req.params;
      axios.get(`https://apis.datos.gob.ar/georef/api/provincias?nombre=${provinceName}`)
      .then((response)=>{
         let data = response.data.provincias[0].centroide;
         res.status(200).send({data});
      })
      .catch(error=>{
         res.status(500).send({error})
      })
}

module.exports = {
   getLatLang
}