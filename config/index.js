const dotenv = require('dotenv');
dotenv.config();

exports.enviroment = process.env || 'developtment';
exports.database_host = process.env.DATABASE_HOST;
exports.database_name = process.env.DATABASE_NAME;
exports.database_username = process.env.DATABASE_USERNAME;
exports.database_password = process.env.DATABASE_PASSWORD;
exports.secret_key = process.env.SECRET_KEY;
exports.datos_base_url = process.env.DATOS_BASE_URL;