const { 
   database_host,
   database_name,
   database_username,
   database_password
 } = require('../config');

module.exports = {
   "development": {
     username: database_username,
     password: database_password,
     database: database_name,
     host: database_host,
     dialect: "mysql"
   }
 }