var express = require('express');
var router = express.Router();

const { signInUser } = require('../controllers/users');

router.post('/signIn', signInUser);

module.exports = router;
