var express = require('express');
var router = express.Router();

const jwtCheck = require('../middlewares/jwtCheck');
const { getLatLang } = require('../controllers/provinces');

router.get('/:provinceName',jwtCheck,getLatLang);

module.exports = router;
