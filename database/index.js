const { Sequelize } = require('sequelize');
const { 
   database_host,
   database_name,
   database_username,
   database_password
 } = require('../config');

 const sequelize = new Sequelize(database_name, database_username, database_password, {
   host: database_host,
   dialect: 'mysql'
});

module.exports = sequelize;