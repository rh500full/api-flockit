const app = require('../app');
const request = require('supertest');
let token = '';

beforeAll(async () => {
   let response = await request(app)
   .post('/users/signIn')
   .send({
      "userName": "rh500full",
      "password": "1234"
   });
   token = response.token;
});

describe('GET /province:provinceName',()=>{
   test('should return an object with the lat and lang of province', async()=>{
      let response = await request(app)
         .get(`/provinces/San Luis`)
         .set('Authorization', token);
      let data = response; 
      expect(data).toHaveProperty('data')
   })
})