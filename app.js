const express = require('express');
var fs = require('fs');
const morgan = require('morgan');
var path = require('path');

const provinceRouter = require('./routes/provinces');
const usersRouter = require('./routes/users');

const errorHandler = require('./middlewares/errorHandler');

const app = express();

app.use(morgan('common', {
   stream: fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
 }))
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/provinces', provinceRouter);
app.use('/users', usersRouter);


app.use(errorHandler);

module.exports = app;
